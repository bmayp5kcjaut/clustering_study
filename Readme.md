Name
====
N-dim clustering Pajek

Overview
多項目のデータを持つ要素の関係をPajekで可視化するためのプログラム

## Description
データの準備　(全ての項目を０－１．０に標準化する)
　デモンストレーション用のデータを作成するためには、create_data.rb を実行
caclcdistance_data.rb
  コードを調整して、適切なグループになるよう、また、
-> pajek_file.net  が作成される

Pajekを起動　(DesktopのPaリンクをクリック)
File -> Read -> *.netを選択
Draw -> Network
Layout -> Kamada-Kawai -> Separate Components
Options -> Mark Vertices Using の各項目で、表示上の色の設定などを行う

## Demo
pajek_file.net

## VS. 

## Requirement
Ubuntu 16.04 
Ruby
Pajek (wine)

## Usage

## Install

## Contribution

## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)

## Author

[tcnksm](https://github.com/tcnksm)
