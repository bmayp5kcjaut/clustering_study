require 'csv'

i=0
mdimdata=Hash.new
CSV.foreach("basic_data.csv") do |row|
	dno=row[0].to_i
	bdata= row[1].to_f
	arr=[]
	(1..10).to_a.each{|col|
		rndno=bdata+(rand()*0.3-0.15)
		if rndno > 1 then
			arr << 1
		elsif rndno < 0 then
			arr <<0
		else
			arr << rndno
		end
	}
	mdimdata[dno]=arr
	i+=1
end
p mdimdata
CSV.open("model_data.csv","w") do |csv|
	mdimdata.each{|dno,data|
		csv << [dno,data].flatten
	}
end