require 'csv'

def edist(d1,d2)
	qsum=0
	d1.zip(d2).each{|set|
		# p (set[1]-set[0])*(set[1]-set[0])
		qsum+=(set[1]-set[0])*(set[1]-set[0])
	}
	# p "#{d1} #{d2} #{qsum}"
	return Math.sqrt(qsum)
end

i=0
mdimdata=Hash.new
CSV.foreach("model_data.csv") do |row|
	mdimdata[row[0].to_i]=row[1,10].map{|x| x.to_f}
	
end
dist=Hash.new

mdimdata.each{|pno1,data1|
	mdimdata.each{|pno2,data2|
		dist[[pno1,pno2]]=edist(data1,data2)
	}
}
p dist

CSV.open("model_dist.csv","w") do |csv|
	csv << ["",(1..30).to_a].flatten
	(1..30).to_a.each{|x|
		arr=[]
		
			(1..30).to_a.each{|y|
				if x < y then
					arr << dist[[x,y]]
				else 
					arr << ""
				end
			}
			csv << [x,arr].flatten

	}
end

near=Hash.new
(1..30).to_a.each{|x|
	(1..30).to_a.each{|y|
		if x < y && dist[[x,y]] < 0.8 then
			near[[x,y]] = 1
		end
	}
}

File.open("pajek_file.net","w") do |file|
	file.puts("*Vertices 30")
	(1..30).to_a.each{|x|
		file.puts("#{x} #{x}")
	}
	file.puts("*Edges")
	near.each{|set,dist|
		p set
		file.puts("#{set[0]} #{set[1]} #{dist}")
	}
end
